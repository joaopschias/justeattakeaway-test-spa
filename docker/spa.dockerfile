FROM node:10.23.0-alpine

WORKDIR /spa

COPY package.json /spa

RUN npm install

COPY . /spa

CMD ["npm", "start"]
