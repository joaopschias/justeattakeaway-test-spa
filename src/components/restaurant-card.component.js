import * as React from 'react';
import {useState} from 'react';
import '../pages/restaurants.page.scss';

const RestaurantCard = ({name, status, isFav, setFav}) => {
    const [favorite, setFavorite] = useState(isFav !== 0);

    const changeFavorite = () => {
        setFav();
        setFavorite(!favorite);
    };

    return (
        <div className={"restaurants-item"}>
            <i onClick={changeFavorite} className={favorite ? "fas fa-star" : "far fa-star"}/>
            <div className={"d-flex flex-column"}>
                <span className={"restaurants-item__name"}>{name}</span>
                <span className={`restaurants-item__status ${status}`}>{status}</span>
            </div>
        </div>
    );
};

export default RestaurantCard;
