import './App.css';
import RestaurantList from './pages/restaurants.page';

function App() {
  return (
      <RestaurantList></RestaurantList>
  );
}

export default App;
