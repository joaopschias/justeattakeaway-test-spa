# Just Eat Takeaway Test - SPA
The goal of this test is to implement a sample project, where you visualize a restaurant list.

## Requirements
* [Git](https://git-scm.com/)
* [Docker](https://www.docker.com/)

## Installation
1 - First, clone repository:

```
$ git clone git@gitlab.com:joaopschias/justeattakeaway-test-spa.git
$ cd justeattakeaway-test-spa
```

2 - If you need to develop some code, switch to the branch develop:

*This step is not required and you can test the project using the branch master*

```
$ git checkout develop
$ git pull
```

3 - Initialize Docker:

*This step can take a few minutes to be done*

```
$ docker-compose up -d
```

## Accessing the Docker server for the SPA

Server: http://localhost:3000/

## Extras

Command to stop Docker and clear all containers:

```
$ docker-compose down
$ docker rm $(docker ps -a -q); docker rmi $(docker images -q);
```

## Framework
[ReactJS](https://reactjs.org)

## Authors

* **João P. Schias** - [LinkedIn](https://www.linkedin.com/in/joaopschias) - [GitLab](https://gitlab.com/joaopschias)
